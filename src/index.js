// ------------ DEPENDENCIES ------------- //

import React from 'react';
import ReactDOM from 'react-dom';

// ------------ LOCAL DEPENDENCIES ------------- //

// CSS.

import './Global.css';

// Components.

import App from './components/Pages/App';
import * as serviceWorker from './serviceWorker';

// ------------ MAIN ------------- //

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
