// ------------- DEPENDENCIES -------------- //

import { combineReducers } from 'redux';

// ------------- LOCAL DEPENDENCIES -------------- //

import sessionReducer from './session';

// ------------- MAIN -------------- //

const rootReducer = combineReducers({
  sessionState          : sessionReducer,
});

export default rootReducer;
