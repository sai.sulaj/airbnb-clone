// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './GridCardItem.css';

// --------------- CONSTANTS --------------- //



// --------------- MAIN --------------- //

class GridCardItem extends Component {
  render() {
    const {
      title,
      to,
      image,
    } = this.props;

    return (
      <div className='grid-card-item-root'>
        <img className='grid-card-item-image' src={image} alt='#' />
        <NavLink className='grid-card-item-title' to={to || '#'}>{title}</NavLink>
      </div>
    );
  }
}

export default GridCardItem;
