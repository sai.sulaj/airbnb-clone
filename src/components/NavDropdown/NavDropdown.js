// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './NavDropdown.css';

import * as Routes from '../../constants/Routes';

// --------------- CONSTANTS --------------- //

const openStyles = {
  height: '100vh',
};

const closedStyles = {
  height: 0,
};

// --------------- MAIN --------------- //

class NavDropdown extends Component {
  render() {
    const { open } = this.props;

    return (
      <div className='navbar-dropdown-root' style={open ? openStyles : closedStyles}>
        <div className='navbar-dropdown-inner'>
          <NavLink className='navbar-dropdown-link-item' to={Routes.HOST_A_HOME}>Home</NavLink>
          <hr className='horizontal-rule' />
          <NavLink className='navbar-dropdown-link-item' to={Routes.HOST_A_HOME}>Invite friends</NavLink>
          <NavLink className='navbar-dropdown-link-item' to={Routes.HOST_A_HOME}>Refer hosts</NavLink>
          <NavLink className='navbar-dropdown-link-item' to={Routes.HOST_A_HOME}>Airbnb for work</NavLink>
          <hr className='horizontal-rule' />
          <NavLink className='navbar-dropdown-link-item' to={Routes.HOST_A_HOME}>List your place</NavLink>
          <NavLink className='navbar-dropdown-link-item' to={Routes.HOST_A_HOME}>Learn about hosting your home</NavLink>
          <NavLink className='navbar-dropdown-link-item' to={Routes.HOST_A_HOME}>Host an experience</NavLink>
          <hr className='horizontal-rule' />
          <NavLink className='navbar-dropdown-link-item' to={Routes.HOST_A_HOME}>Help</NavLink>
          <NavLink className='navbar-dropdown-link-item' to={Routes.HOST_A_HOME}>Sign up</NavLink>
          <NavLink className='navbar-dropdown-link-item' to={Routes.HOST_A_HOME}>Log in</NavLink>
        </div>
      </div>
    );
  }
}

export default NavDropdown;
