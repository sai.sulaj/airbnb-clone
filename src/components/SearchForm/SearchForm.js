// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';


// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './SearchForm.css';

// Components.

import InputField from '../InputField';
import InputDateField from '../InputDateField';
import Button from '../Button';

// --------------- CONSTANTS --------------- //

const mobileFormStyles = {};
const desktopFormStyles = {
  borderRadius: '0 0 4px 4px',
};


// --------------- MAIN --------------- //

class SearchForm extends Component {
  render() {
    const {
      classes,
      whereInputValue,
      checkInInputValue,
      checkOutInputValue,
      guestsInputValue,
      onChange,
      isMobile,
      uid,
    } = this.props;

    const classesP = classes === undefined ? '' : classes;

    return (
      <form className={`search-form-root ${classesP}`}
        style={isMobile ? mobileFormStyles : desktopFormStyles}>
        <InputField isMobile={isMobile} label='WHERE' id='where-input' placeholder='Anywhere'
          value={whereInputValue} borderRadius='3px'
          onChange={(value) => onChange('whereInputValue', value)}/>
        <div className={`search-form-inline-input-wrapper`}>
          <InputDateField uid={uid * 2 + 1} label='CHECK IN' id='check-in-input' placeholder='dd-mm-yyyy'
            value={checkInInputValue} borderRadius='3px 0 0 3px'
            onChange={(value) => onChange('checkInInputValue', value)} readOnly={true}/>
          <InputDateField uid={uid * 2 + 2} label='CHECK OUT' id='check-out-input' placeholder='dd-mm-yyyy'
            value={checkOutInputValue} borderRadius='0 3px 3px 0' selectedDateBefore={checkInInputValue}
            onChange={(value) => onChange('checkOutInputValue', value)} isRange={true} handleNarrow={true} readOnly={true}/>
        </div>
        <InputField isMobile={isMobile} label='GUESTS' id='guests-input' placeholder='Guests'
          value={guestsInputValue} borderRadius='3px' type='number'
          onChange={(value) => onChange('guestsInputValue', value)}/>
        <Button classes='search-form-button' label='Search' />
      </form>
    );
  }
}

export default SearchForm;
