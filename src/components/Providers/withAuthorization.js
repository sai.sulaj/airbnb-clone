// ------------- DEPENDENCIES -------------- //

import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';

// ------------- LOCAL DEPENDENCIES -------------- //

// Constants.

import * as Routes from '../../constants/Routes';

// Components.

import { Firebase } from '../../firebase';

// ------------- MAIN -------------- //

/**
 * Function that returns component only if authCondition is passed.
 *
 * @param {function(*): Boolean} authCondition -> Condition required to
 *                                                access component.
 *
 * @return {React.Component} -> Component that is requested.
 */
const withAuthorization = authCondition => Component => {
  class WithAuthorization extends React.Component {
    componentDidMount = () => {
      const { history } = this.props;

      Firebase.FirebaseAuth.onAuthStateChanged(authUser => {
        if (!authCondition(authUser)) {
          history.push(Routes.SIGN_UP);
        }
      });
    };

    render() {
      const { authUser, ...other } = this.props;

      return authUser ? <Component {...other} />: null;
    }
  }

  const mapStateToProps = state => ({
    authUser: state.sessionState.authUser,
  });

  return compose(
    withRouter,
    connect(mapStateToProps),
  )(WithAuthorization);
};

export default withAuthorization;
