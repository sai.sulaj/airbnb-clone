// ------------- DEPENDENCIES -------------- //

import React from 'react';
import { connect } from 'react-redux';

// ------------- LOCAL DEPENDENCIES -------------- //

// Constants.

import * as Actions from '../../constants/ActionTypes';

// Components.

import { Firebase } from '../../firebase';

// ------------- MAIN -------------- //

/**
 * Component wrapper that providers authUser object when available to
 * component.
 *
 * @param {React.Component} Component -> Arbitrary component that requires
 *                                       authentication to access.
 *
 * @return {React.Component} Component -> Passed component wrapped with
 *                                        helper.
 */
const withAuthentication = Component => {
  class WithAuthentication extends React.Component {
    componentDidMount = () => {
      const { onSetAuthUser } = this.props;

      Firebase.FirebaseAuth.onAuthStateChanged(authUser => {
        authUser ? onSetAuthUser(authUser): onSetAuthUser(null);
      });
    };

    render() {
      return <Component />;
    }
  }

  const mapDispatchToProps = dispatch => ({
    onSetAuthUser: authUser =>
      dispatch({
        type    : Actions.AUTH_USER_SET,
        authUser: authUser,
      }),
  });

  return connect(
    null,
    mapDispatchToProps,
  )(WithAuthentication);
};

export default withAuthentication;
