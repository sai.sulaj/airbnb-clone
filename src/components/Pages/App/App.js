// --------------- DEPENDENCIES --------------- //

import React, { PureComponent } from 'react';
import {
  Route,
  BrowserRouter as Router,
} from 'react-router-dom';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './App.css';

// Constants.

import * as Routes from '../../../constants/Routes';

// Pages.

import Landing from '../Landing';

// --------------- MAIN --------------- //

class App extends PureComponent {
  render() {
    return (
      <Router className='app-root'>
        <Route exact
          path={Routes.LANDING}
          component={Landing} />
      </Router>
    );
  }
}

export default App;
