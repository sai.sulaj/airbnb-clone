// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';


// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './Landing.css';

// Components.

import Navbar from '../../NavBar';
import SearchForm from '../../SearchForm';
import GridContainer from '../../GridContainer';
import GridCardItem from '../../GridCardItem';

// Media.

import ElevatedHouseInForestImage from '../../../assets/images/elevated-house-in-forest.jpg';
import WhiteBedroomBedImage from '../../../assets/images/white-bedroom-bed.jpg';
import WomenRidingBikesImage from '../../../assets/images/women-riding-bikes.jpg';
import FoodOnTableImage from '../../../assets/images/food-on-table.jpg';

// --------------- CONSTANTS --------------- //


const INIT_STATE = {
  isMobile          : false,
  whereInputValue   : '',
  checkInInputValue : null,
  checkOutInputValue: null,
  guestsInputValue  : '',
};

const mobileFormTitleStyles = {
  backgroundColor: 'transparent',
  position       : 'absolute',
  bottom         : 25,
  left           : 25,
  color          : '#FFFFFF',
  fontSize       : 25,
};
const desktopFormTitleStyles = {
  backgroundColor: '#FFFFFF',
  padding: '32px 32px 8px 32px',
};

// --------------- MAIN --------------- //

const FormTitle = ({ isMobile }) => (
  <h1 className='landing-form-title'
    style={ isMobile ? mobileFormTitleStyles : desktopFormTitleStyles}>
    Book unique homes and experiences.
  </h1>
);

const Form = ({
  isMobile,
  whereInputValue,
  checkInInputValue,
  checkOutInputValue,
  guestsInputValue,
  handleFormChange,
  uid,
}) => (
  <div className={`${isMobile ? 'landing-search-form-mobile' : 'landing-search-form-desktop'} landing-search-form-root`}>
    {!isMobile && <FormTitle isMobile={isMobile} /> }
    <SearchForm isMobile={isMobile} uid={uid}
      onChange={handleFormChange}
      classes='landing-search-form'
      whereInputValue={whereInputValue}
      checkInInputValue={checkInInputValue}
      checkOutInputValue={checkOutInputValue}
      guestsInputValue={guestsInputValue} />
  </div>
);

const dummyGridData = [
  {
    title: 'Homes',
    image: WhiteBedroomBedImage,
  },
  {
    title: 'Experiences',
    image: WomenRidingBikesImage,
  },
  {
    title: 'Restaurants',
    image: FoodOnTableImage,
  },
];

class Landing extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  componentDidMount = () => {
    this.updateIsMobile();
    window.addEventListener('resize', this.updateIsMobile);
  }

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.updateIsMobile);
  }
  
  updateIsMobile = () => {
    const { isMobile } = this.state;
    if (window.innerWidth > 1124 && isMobile) this.setState({ isMobile: false, });
    else if (window.innerWidth <= 1124 && !isMobile) this.setState({ isMobile: true, });
  }

  handleFormChange = (label, value) => {
    this.setState({ [label]: value });
  }

  render() {
    const {
      isMobile,
    } = this.state;

    return (
      <div className='landing-root'>
        <Navbar isMobile={isMobile} classes='landing-navbar' />
        {isMobile &&
          <React.Fragment>
            <div className='landing-hero-image-wrapper'>
              <img className='landing-hero-image landing-hero-image-mobile' src={ElevatedHouseInForestImage} alt='elevated-house-in-forest' />
              <FormTitle isMobile={isMobile}/>
            </div>
            <Form uid={0} handleFormChange={this.handleFormChange} {...this.state} />
          </React.Fragment>
        }
        {!isMobile &&
          <div className='landing-hero-image-wrapper landing-hero-image-wrapper-desktop'>
            <img className='landing-hero-image landing-hero-image-desktop' src={ElevatedHouseInForestImage} alt='elevated-house-in-forest' />
            <Form uid={1} handleFormChange={this.handleFormChange} {...this.state} />
          </div>
        }
        <div className='experience-wrapper'>
          <h3 className='experience-header'>Explore Airbnb</h3>
          <GridContainer gridData={dummyGridData} GridItemComponent={GridCardItem} />
        </div>
      </div>
    );
  }
}

export default Landing;
