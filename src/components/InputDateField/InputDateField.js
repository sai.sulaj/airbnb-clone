// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import _ from 'lodash';
import Moment from 'moment';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './InputDateField.css';

// Media.

import RightArrow from '../../assets/right-arrow.svg';
import LeftArrow from '../../assets/left-arrow.svg';

// Components.

import InputField from '../InputField';
import OutsideClickListener from '../OutsideClickListener';

// --------------- CONSTANTS --------------- //

const TOTAL_CELLS = 35;
const MONTH_NAMES = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
const DAY_OF_WEEK_NAMES = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];

const INIT_STATE = {
  currentDate  : null,
  activeDate   : null,
  dropdownOpen : false,
  firstMonthDay: null,
  lastMonthDay : null,
  selectedDate : null,
};

const BORDER_COLOR                    = 'rgb(228, 231, 231)';
const TRANSPARENT                     = 'transparent';
const SELECTED_DATE_BACKGROUND_COLOR  = 'rgb(0, 166, 153)';
const BLACK                           = '#000000';
const SELECTED_DATE_COLOR             = '#FFFFFF';
const INACTIVE_DATE_COLOR             = 'rgb(228, 231, 231)';
const SELECTED_BORDER_COLOR           = 'rgb(0, 166, 153)';
const SELECTED_RANGE_BACKGROUND_COLOR = 'rgb(51, 218, 205)';

// --------------- MAIN --------------- //

const getMonthData = (_date) => {
  let activeDate = null;
  if (_date !== undefined && _date instanceof Moment) {
    activeDate = _date;
  } else {
    activeDate = Moment(new Date());
  }
  const firstDay = activeDate.startOf('month').clone().day();
  const lastDay = activeDate.endOf('month').clone().date();
  return {
    firstMonthDay: firstDay,
    lastMonthDay: lastDay,
  };
}

const CalendarDropdown = ({
  month,
  year,
  firstDay,
  lastDay,
  onRightButtonClick,
  onLeftButtonClick,
  onDateSelect,
  selectedDate,
  currentDate,
  isRange,
  selectedDateBefore,
  handleNarrow,
}) => (
  <div className={`input-date-dropdown-root ${handleNarrow ? 'input-date-dropdown-handle-narrow' : ''}`}>
    <div className='input-date-dropdown-grid'>
      <div className="input-date-dropdown-header-container">
        <img src={LeftArrow} className='input-date-dropdown-calendar-buttons'
              onClick={() => onLeftButtonClick()} alt='left-button-click' />
        <span>{`${month} ${year}`}</span>
        <img src={RightArrow} className='input-date-dropdown-calendar-buttons'
              onClick={() => onRightButtonClick()} alt='right-button-click' />
      </div>
      <div className="input-date-dropdown-dow-container">
        <DOWs />
      </div>
      <CalendarCells currentDate={currentDate} selectedDate={selectedDate} leftOffset={firstDay}
                    rightOffset={lastDay} onSelect={onDateSelect} isRange={isRange} selectedDateBefore={selectedDateBefore}
                    activeYearNumber={year} activeMonthNumber={MONTH_NAMES.indexOf(month)} />
    </div>
  </div>
);

const DOWs = () => (
  <React.Fragment>
    {_.range(7).map(i => (
      <span key={i}>{DAY_OF_WEEK_NAMES[i]}</span>
    ))}
  </React.Fragment>
);

const isCellSelected = (selectedDate, activeYearNumber, activeMonthNumber, date, tmpDate) => {
  if (selectedDate !== null) {
    tmpDate.year(activeYearNumber).month(activeMonthNumber).date(date);
    const isSame = tmpDate.isSame(selectedDate, 'day');
    return isSame;
  }
  return false;
}

const isCellInactive = (currentDate, activeYearNumber, activeMonthNumber, date, isRange, tmpDate) => {
  if (currentDate !== null) {
    tmpDate.year(activeYearNumber).month(activeMonthNumber).date(date);
    const isBefore = tmpDate.add(isRange ? -1 : 0, 'days').isBefore(currentDate, 'day');
    return isBefore;
  }
  return false;
}

const isCellInSelectedRange = (selectedDateBefore, selectedDate, date, tmpDate, activeMonthNumber, activeYearNumber) => {
  if (selectedDateBefore !== null && selectedDateBefore !== undefined && selectedDate !== null) {
    tmpDate.year(activeYearNumber).month(activeMonthNumber).date(date);
    const isInRange = selectedDateBefore.isBefore(tmpDate) && tmpDate.isBefore(selectedDate);
    return isInRange;
  }
  return false;
}

const isCellBeforeSelectedBefore = (selectedDateBefore, date, tmpDate, activeMonthNumber, activeYearNumber) => {
  if (selectedDateBefore !== null && selectedDateBefore !== undefined) {
    tmpDate.year(activeYearNumber).month(activeMonthNumber).date(date);
    const isBeforeSelectedBefore = tmpDate.isSameOrBefore(selectedDateBefore);
    return isBeforeSelectedBefore;
  }
  return false;
}

const CalendarCells = ({
    leftOffset,
    rightOffset,
    onSelect,
    selectedDate,
    currentDate,
    isRange,
    activeYearNumber,
    activeMonthNumber,
    selectedDateBefore,
  }) => {
  const tmpDate      = Moment();
  const getDate      = i => i - leftOffset + 2;
  const isDate       = i => (getDate(i) > 0) && (getDate(i) <= rightOffset);
  const leftBorder   = i => ((i % 7 === 0) && isDate(i)) || (getDate(i) === 1) ? BORDER_COLOR : TRANSPARENT;
  const rightBorder  = i => isDate(i) ? BORDER_COLOR : TRANSPARENT;
  const topBorder    = i => isDate(i) ? BORDER_COLOR : TRANSPARENT;
  const bottomBorder = i => isDate(i) && getDate(i) > rightOffset - 7 ? BORDER_COLOR : TRANSPARENT;
  const isSelected   = i => isCellSelected(selectedDate, activeYearNumber, activeMonthNumber, getDate(i), tmpDate)
                            || (isRange && isCellSelected(selectedDateBefore, activeYearNumber, activeMonthNumber, getDate(i), tmpDate));
  const isInactive   = i => isCellInactive(currentDate, activeYearNumber, activeMonthNumber, getDate(i), isRange, tmpDate);
  const isInSelectedRange = i => isCellInSelectedRange(selectedDateBefore, selectedDate, getDate(i), tmpDate, activeMonthNumber, activeYearNumber);
  const beforeSelectedDate = i => isRange && isCellBeforeSelectedBefore(selectedDateBefore, getDate(i), tmpDate, activeMonthNumber, activeYearNumber);
  return (
    <React.Fragment>
      {_.range(TOTAL_CELLS).map((z, i) => {
        const borderColor = isSelected(i) && isDate(i) ? SELECTED_BORDER_COLOR : isInSelectedRange(i) && isDate(i) ? SELECTED_RANGE_BACKGROUND_COLOR :  `${topBorder(i)} ${rightBorder(i)} ${bottomBorder(i)} ${leftBorder(i)}`;
        return (
          <div key={i} className={`input-date-dropdown-cell-container`}
            onClick={() => isDate(i) && !isInactive(i) && !beforeSelectedDate(i) && onSelect(getDate(i))}
            style={{
              borderColor,
              backgroundColor: isSelected(i) && isDate(i) ? SELECTED_DATE_BACKGROUND_COLOR               : isInSelectedRange(i) && isDate(i) ? SELECTED_RANGE_BACKGROUND_COLOR: TRANSPARENT,
              color          : (isSelected(i) || isInSelectedRange(i)) && isDate(i) ? SELECTED_DATE_COLOR: isInactive(i) ? INACTIVE_DATE_COLOR                                : BLACK,
              cursor         : isInactive(i) || beforeSelectedDate(i) ? 'unset'                          : 'pointer',
            }}>
            {isDate(i) && getDate(i)}
          </div>
          );
      })}
    </React.Fragment>
  )
};

class InputDateField extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  componentDidMount = () => {
    this.setState({
      activeDate : Moment(new Date()),
      currentDate: Moment(new Date()),
      ...getMonthData(),
    });
  }

  handleOutsideClick = () => {
    const { dropdownOpen } = this.state;
    if (dropdownOpen) {
      this.setState({ dropdownOpen: false, });
    }
  }

  handleInputClick = () => {
    this.setState({ dropdownOpen: true, });
  }

  handleRightButtonClick = () => {
    this.setState(prevState => {
      const nextDate = prevState.activeDate.clone();
      nextDate.add(1, 'months');
      return {
        activeDate: nextDate,
        ...getMonthData(nextDate),
      }
    });
  }

  handleLeftButtonClick = () => {
    this.setState(prevState => {
      const nextDate = prevState.activeDate.clone();
      nextDate.add(-1, 'months');
      return {
        activeDate: nextDate,
        ...getMonthData(nextDate),
      }
    });
  }

  handleDateSelect = (dayOfMonth) => {
    const { activeDate } = this.state;
    const { onChange } = this.props;
    const newSelectDate = activeDate.clone().date(dayOfMonth);
    this.setState({ selectedDate: newSelectDate, });
    onChange(newSelectDate);
  }

  render() {
    const {
      activeDate,
      firstMonthDay,
      lastMonthDay,
      dropdownOpen,
      selectedDate,
      currentDate,
    } = this.state;
    const {
      uid,
      value,
      isRange,
      selectedDateBefore,
      handleNarrow,
      readOnly,
      ...other
    } = this.props

    let month = null;
    let year = null;

    if (activeDate !== null && activeDate !== undefined) {
      month = MONTH_NAMES[activeDate.month()];
      year = activeDate.year();
    }

    const valueValid = value !== null && value !== undefined && value instanceof Moment;

    return (
      <div className='input-date-root'>
        <InputField onClick={this.handleInputClick} value={valueValid ? value.format('YYYY-MM-DD') : ''} {...other} autoComplete='off' readOnly={readOnly} />
        <OutsideClickListener uid={uid} onOutsideClick={this.handleOutsideClick}>
          {dropdownOpen && <CalendarDropdown month={month} year={year} firstDay={firstMonthDay} lastDay={lastMonthDay}
                                              onRightButtonClick={this.handleRightButtonClick}
                                              onLeftButtonClick={this.handleLeftButtonClick}
                                              onDateSelect={this.handleDateSelect} isRange={isRange}
                                              selectedDate={selectedDate} currentDate={currentDate}
                                              selectedDateBefore={selectedDateBefore} handleNarrow={handleNarrow} />}
        </OutsideClickListener>
      </div>
    );
  }
}

export default InputDateField;
