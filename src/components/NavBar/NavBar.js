// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './NavBar.css';

// Media.

import AirbnbLogoWhite from '../../assets/airbnb-logo-white.svg';
import AirbnbLogoPink from '../../assets/airbnb-logo-pink.svg';
import ArrowDown from '../../assets/arrow-down.svg';
import ArrowUp from '../../assets/arrow-up.svg';

// Constants.

import * as Routes from '../../constants/Routes';

// Components.

import NavDropdown from '../NavDropdown';

// --------------- CONSTANTS --------------- //

const INIT_STATE = {
  dropdownOpen: false,
};

// --------------- MAIN --------------- //

class NavBar extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  handleToggleDropdownClick = () => {
    const { isMobile } = this.props;
    if (isMobile) this.setState(prevState => ({ dropdownOpen: !prevState.dropdownOpen, }));
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.dropdownOpen && !nextProps.isMobile) {
      return {
        dropdownOpen: false,
      };
    }
    return null;
  }

  handleWindowResize = () => {
    const { dropdownOpen } = this.state;
    if (window.innerWidth >= 1000 && dropdownOpen) { this.setState({ dropdownOpen: false, }); }
  }

  render() {
    const {
      dropdownOpen,
    } = this.state;
    const {
      classes,
    } = this.props;

    const classesP = classes === undefined ? '' : classes;

    return (
      <div className={`navbar-root ${classesP}`}>
        <div className='navbar-logo-wrapper' onClick={() => this.handleToggleDropdownClick()} >
          <img src={dropdownOpen ? AirbnbLogoPink : AirbnbLogoWhite} className='navbar-airbnb-logo' alt='airbnb-logo' />
          <img src={dropdownOpen ? ArrowUp : ArrowDown} className='navbar-arrow-down-img' alt='arrow-down' />
        </div>
        <div className='navbar-links-wrapper'>
          <NavLink className='navbar-link-item' to={Routes.HOST_A_HOME}>Become a host</NavLink>
          <NavLink className='navbar-link-item' to={Routes.HOST_A_HOME}>Help</NavLink>
          <NavLink className='navbar-link-item' to={Routes.HOST_A_HOME}>Sign up</NavLink>
          <NavLink className='navbar-link-item' to={Routes.HOST_A_HOME}>Log in</NavLink>
        </div>
        <NavDropdown open={dropdownOpen} />
      </div>
    );
  }
}

export default NavBar;
