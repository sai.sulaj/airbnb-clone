// --------------- DEPENDENCIES --------------- //

import React from 'react';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './InputField.css';

// --------------- CONSTANTS --------------- //



// --------------- MAIN --------------- //

const InputField = ({
    label,
    id,
    type,
    value,
    borderRadius,
    placeholder,
    onChange,
    onClick,
    autoComplete,
    readOnly,
  }) => (
  <div className='input-field-wrapper' onClick={() => onClick !== undefined && onClick()}>
    <label className='input-field-label' htmlFor={id}>
      {label}
    </label>
    <input className='input-field-input' id={id} value={value}
      placeholder={placeholder} type={type}
      onChange={evt => onChange(evt.target.value)}
      autoComplete={autoComplete} readOnly={readOnly}
      onFocus={() => onClick !== undefined && onClick()}
      style={{
        borderRadius,
      }}/>
  </div>
);

export default InputField;
