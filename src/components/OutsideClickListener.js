import React, { Component } from 'react';

/**
 * Component that alerts if you click outside of it
 */
export default class OutsideClickListener extends Component {
  componentDidMount() {
    const { uid } = this.props;
    if (!document.hasOutsideClickListener) {
      document.hasOutsideClickListener = {};
    }
    if (!document.hasOutsideClickListener[uid]) {
      document.addEventListener('mousedown', this.handleClickOutside);
      document.hasOutsideClickListener[uid] = true;
    }
  }

  componentWillUnmount() {
    const { uid } = this.props;
    document.removeEventListener('mousedown', this.handleClickOutside);
    document.hasOutsideClickListener[uid] = false;
  }

  /**
   * Set the wrapper ref
   */
  setWrapperRef = (node) => {
    this.wrapperRef = node;
  }

  /**
   * Alert if clicked on outside of element
   */
  handleClickOutside = (event) => {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      const { onOutsideClick } = this.props;
      onOutsideClick();
    }
  }

  render() {
    return <div ref={this.setWrapperRef}>{this.props.children}</div>;
  }
}