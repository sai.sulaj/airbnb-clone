// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import _ from 'lodash';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './GridContainer.css';

// --------------- CONSTANTS --------------- //



// --------------- MAIN --------------- //

class GridContainer extends Component {
  render() {
    const {
      GridItemComponent,
      gridData,
    } = this.props;

    return (
      <div className='grid-container-root'>
        {gridData.map((data, i) => (
          <GridItemComponent {...data} />
        ))}
      </div>
    );
  }
}

export default GridContainer;
