// --------------- DEPENDENCIES --------------- //

import React from 'react';


// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './Button.css';

// --------------- CONSTANTS --------------- //



// --------------- MAIN --------------- //

const Button = ({
    label,
    onClick,
    classes,
  }) => (
  <button className={`button-root ${classes === undefined ? '' : classes}`}
    onClick={onClick}>
    {label}
  </button>
);

export default Button;
