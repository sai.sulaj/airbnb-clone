// ------------- DEPENDENCIES ------------- //

import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

// ------------- MAIN ------------- //

const config = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const FirebaseAuth = firebase.auth();
const Db = firebase.firestore();
const FacebookAuthProvider = new firebase.auth.FacebookAuthProvider();
FacebookAuthProvider.addScope('ads_management');

const settings = { timestampsInSnapshots: true };
Db.settings(settings);

export { FirebaseAuth, FacebookAuthProvider, Db };
